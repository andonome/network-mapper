# Setup

`scan` depends upon graph-easy (often called `perl-graph-easy`) and arp-scan.

# Example Output

```
                       ┌───────────────┐
                       │   Raspberry   │
                       │ 192.168.1.243 │
                       └───────────────┘
                         ∧
                         │
                         │
                         ∨
┌───────────────┐      ┌───────────────┐      ┌───────────────┐
│     lepht     │      │               │      │    Apple,     │
│ 192.168.1.136 │ <──> │               │ <──> │ 192.168.1.101 │
└───────────────┘      │  192.168.1.1  │      └───────────────┘
┌───────────────┐      │               │      ┌───────────────┐
│    Hewlett    │      │               │      │    Hewlett    │
│ 192.168.1.249 │ <──> │               │ <──> │ 192.168.1.108 │
└───────────────┘      └───────────────┘      └───────────────┘
                         ∧
                         │
                         │
                         ∨
                       ┌───────────────┐
                       │    Hewlett    │
                       │ 192.168.1.163 │
                       └───────────────┘

```

Arp-scan is used to list items on the network, and graph-easy makes and easy graph of the network.

If running arp-scan versions before 1.9.1, replace `-glx` with `-gl`.
