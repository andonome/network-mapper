#!/bin/sh

gate=$(ip route | cut -d" " -f3|head -1)

host="[ $(hostname) \n $(hostname -i) ]<-->[ $gate ]"

scan=$(sudo arp-scan -glx --backoff=4 --retry=2 -t 500 | awk '{print "[" "gateway" "]" "<-->" "[" $3 " " $4 " " $6 "\\n"  $1 "]"}' | grep -v n"$gate]"| sed s/gateway/$gate/g)

map="$host 
$scan"

echo "$map"  |graph-easy --as=boxart

